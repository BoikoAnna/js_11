//Написати реалізацію кнопки "Показати пароль".
//Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

//Технічні вимоги:
//У файлі index.html лежить розмітка двох полів вводу пароля.
//Після натискання на іконку поруч із конкретним полем - повинні відображатися символи,
//які ввів користувач, іконка змінює свій зовнішній вигляд.
// У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
//Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
//Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
//Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
//Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
//Якщо значення не збігаються - вивести під другим полем текст червоного кольору
//Потрібно ввести однакові значення
//Після натискання на кнопку сторінка не повинна перезавантажуватись
//Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.



// Находим все глаза
const eyes = document.querySelectorAll("i");
// переберем все глаза
eyes.forEach((eye) => {
// на каждый глаз повесим прослушку
eye.addEventListener("click", () => {
// тоглим класс fa-eye-slash
eye.classList.toggle("fa-eye-slash");

// получаем input который идет прям перед нашим глазом в html
const input = eye.previousElementSibling;
// переписываем значение type у нашего input
input.type = input.getAttribute("type") === "text" ? "password" : "text";
});
});


let check = function() {
 if (document.getElementById('password').value ==
document.getElementById('confirm_password').value) {
  document.getElementById('message').style.color = 'green';
  document.getElementById('message').innerHTML = 'You are welcome';
  } else {
  document.getElementById('message').style.color = 'red';
  document.getElementById('message').innerHTML = 'Потрібно ввести однакові значення';
   }
  };
